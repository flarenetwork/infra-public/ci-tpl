# Usage

All jobs are defined as so called ["hidden jobs"](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs) so you have full control over them when implementing them and overriding properties.

## Import general lint template

``` yml
include:
- project: 'flarenetwork/infra-team/ci-tpl'
  ref: main
  file: '/lint.yml'
```

## Terraform lint

### Jobs in [`lint.yml`](./lint.yml)

- `.ci-tpl.lint-terraform-global` lints all files ending with `.tf(vars)?` in the repo
- `.ci-tpl.lint-terraform-mr` only lints files ending with `.tf(vars)?` changed in MR

### Environment variables

| Variable | Default | |
|---|---|---|
| `LINT_TERRAFORM_IMAGE` | `registry.gitlab.com/flarenetwork/infra-team/ci-tpl/lint-terraform:latest` | The image used in CI jobs |
| `LINT_OUTPUT_HTML` | `${CI_PROJECT_DIR}/lint-terraform-output.html` | Filepath to which the generated html file is written |

### `.gitlab.ci.yml`

Make sure you import the [general lint template](#import-general-lint-template).

``` yml
my-custom-tflint-job-for-mr-changed-files:
  extends: .ci-tpl.lint-terraform-mr

my-custom-tflint-job-fullrepo:
  extends: .ci-tpl.lint-terraform-global
```

## Shell lint

### Jobs in [`lint.yml`](./lint.yml)

- `.ci-tpl.lint-shell-global` lints all files ending with `.sh`

### Environment variables

| Variable | Default | |
|---|---|---|
| `LINT_SHELL_IMAGE` | `registry.gitlab.com/flarenetwork/infra-team/ci-tpl/lint-shell:latest` | The image used in CI jobs |

### `.gitlab.ci.yml`

Make sure you import the [general lint template](#import-general-lint-template).

``` yml
shell-lint-fullrepo:
  extends: .ci-tpl.lint-shell-global
```

## Dockerfile lint

### Jobs in [`lint.yml`](./lint.yml)

- `.ci-tpl.lint-dockerfile-global` lints all files ending with `.dockerfile`
- `.ci-tpl.lint-dockerfile-mr` only lints files ending with `.dockerfile` changed in MR

### Environment variables

| Variable | Default | |
|---|---|---|
| `LINT_SHELL_IMAGE` | `registry.gitlab.com/flarenetwork/infra-team/ci-tpl/lint-dockerfile:latest` | The image used in CI jobs |

### `.gitlab.ci.yml`

Make sure you import the [general lint template](#import-general-lint-template).

``` yml
dockerfiles-lint-fullrepo:
  extends: .ci-tpl.lint-dockerfile-global
```
