FROM koalaman/shellcheck-alpine:latest@sha256:0c04e4218871bf9cc0168557c22aeb450a0f1c49a2e8ec7f3d68e4c7017f75ed

RUN apk add --no-cache git=2.40.1-r0 colordiff=1.0.21-r0 python3=3.11.5-r0 bash=5.2.15-r5
COPY lint-diff2html.py lint-entrypoint.sh /
RUN chmod +x /lint-entrypoint.sh

ENTRYPOINT [ "bash", "-c", "/lint-entrypoint.sh" ]