FROM ghcr.io/hadolint/hadolint:latest-alpine@sha256:e6f3fb90751bd85e53e4f0b758fbd77bce25833c80d9dcdb49aabc5ee39ff018

RUN apk add --no-cache git=2.34.8-r0 bash=5.1.16-r0
COPY lint-entrypoint.sh /
RUN chmod +x /lint-entrypoint.sh

ENTRYPOINT [ "bash", "-c", "/lint-entrypoint.sh" ]
