#!/usr/bin/python3
import html
import sys
import os
import re





# BASIC VALIDATION FOR FIRST AND SECOND ARGUMENT
_filerror=False
try:
    sys.argv[1]
    if not os.path.exists(os.path.realpath(sys.argv[1])):
        print("File '%s' does not exist" % os.path.realpath(sys.argv[1]))
        _filerror=True
except IndexError:
    print("First argument must be the file containing the output of 'diffreport fmt -check -diff'")
    _filerror=True

try:
    sys.argv[2]
    if not os.path.isdir(os.path.dirname(os.path.realpath(sys.argv[2]))):
        print("Directory '%s' does not exist" % os.path.dirname(os.path.realpath(sys.argv[2])))
        _filerror=True
except IndexError:
    print("Second argument must be the output file where the html page will be written to")
    _filerror=True

if _filerror:
    print("\nUsage: ./lint-diff2html.py <diffreport format output file> <html output file>")
    exit(2)




def append_multiple_lines(file_name, lines_to_append):
    # Open the file in append & read mode ('a+')
    with open(file_name, "a+") as file_object:
        appendEOL = False
        # Move read cursor to the start of file.
        file_object.seek(0)
        # Check if file is not empty
        data = file_object.read(100)
        if len(data) > 0:
            appendEOL = True
        # Iterate over each string in the list
        for line in lines_to_append:
            # If file is not empty then append '\n' before first line for
            # other lines always append '\n' before appending line
            if appendEOL == True:
                file_object.write("\n")
            else:
                appendEOL = True
            # Append element at the end of file
            file_object.write(line)






# Write the head of the html file
with open(os.path.realpath(sys.argv[2]), "wt") as html_file:
    html_file.write("""
    <html>
        <head>
            <title>lint report</title>
        </head>
        <style>
/*
Using theme called "Gogh" from https://gogh-co.github.io/Gogh/
*/


html, body {
    margin: 0;
    padding: 0;
    display: grid;
    background-color: #292D3E;
    color: #FFFEFE;
    font-family: monospace;
}
body {
    min-height: 100vh;
    grid-auto-rows: auto;
    row-gap: 1rem;
}

nav {
    grid-row: 1;
    display: grid;
    grid-auto-columns: auto;
    grid-auto-flow: column;
    column-gap: 1rem;
    padding: .5rem;
    margin: 0 auto auto;
    border: 1px solid #676E95;
    border-top: none;
    border-bottom-left-radius: .4rem;
    border-bottom-right-radius: .4rem;
    background-color: #676E9525;
}
nav span {
    margin: auto;
}
nav input {
    padding: 4px 10px;
    outline: none;
}
nav a {
    color: #F580FF;
    margin: auto;
    text-decoration: none;
}

.file {
    display: grid;
    margin-left: 2rem;
    border: 1px dashed #ABB2BF;
    border-radius: 1rem;
}
.file span.filepath {
    font-size: large;
    font-weight: bold;
    padding: 1rem;
    cursor: pointer;
}
.file.collapsed .diff {
    display: none;
}
.file .diff {
    overflow-x: auto;
    margin: 0 1rem 1rem;
    padding-top: 1rem;
    border-top: 1px dashed #ABB2BF;
}
.file .diff > pre {
    margin: 0;
}
.file .diff > pre span {
    color: var(--color);
}



.noselect {
    -webkit-user-select: none; /* Safari */
    -ms-user-select: none; /* IE 10 and IE 11 */
    user-select: none; /* Standard syntax */  
}
        </style>
        <body>
    """)


# Add content to the html file
with open(os.path.realpath(sys.argv[1])) as diffreport_lint_output_file:
    allLintOutputContents = diffreport_lint_output_file.read()
    diffFilesMatches=re.split(r"^(\S+\n--- old\S+\n\+{3} new\S+)$", allLintOutputContents, flags=re.MULTILINE)[1:]

    # Validate that there's always the diff "header" and the "contents" that can be paired into groups
    if len(diffFilesMatches) % 2 != 0:
        print("Unexpected error! Amount of 'diff chunks' is not an even number!")
        exit(1)

    diffFileGroups = [diffFilesMatches[n:n+2] for n in range(0, len(diffFilesMatches), 2)]


    # Add navbar
    with open(os.path.realpath(sys.argv[2]), "a+") as html_file:
        navString = """<nav><input id="findyourfile" placeholder="Search by file path" type="text"></input><span>%s files in total</span>""" % (len(diffFileGroups))
        if (os.environ.get("CI_JOB_URL")):
            navString += """<a href="%s">Job #%s</a>""" % (os.environ.get("CI_JOB_URL"), os.environ.get("CI_JOB_ID"))
        navString += "</nav>"
        html_file.write(navString)

    # Add content to the html file
    with open(os.path.realpath(sys.argv[2]), "a+") as html_file:

        # Add files themself
        for diffFileGroup in diffFileGroups:
            diff_file = diffFileGroup[0].split('\n')[0]

            lines_html_string = "<pre>"

            for line in diffFileGroup[1].split("\n"):
                if len(line) < 2:
                    continue

                classtyle = ""
                if line[0] == "+":
                    classtyle = "--color:#62DE84;"
                elif line[0] == "-":
                    classtyle = "--color:#F07178;"
                elif line[0] == "@":
                    classtyle = "--color:#60BAEC;user-select:none;-webkit-user-select:none;"
                elif line[0] == "\\":
                    classtyle = "--color:#ABB2BF;user-select:none;-webkit-user-select:none;"

                lines_html_string += """<span style="%s" class="noselect">%s</span><span style="%s">%s</span>\n""" % (classtyle, html.escape(line[0]), classtyle, html.escape(line[1:]))
            
            lines_html_string += "</pre>"

            html_file.write("""<div class="file" filepath="%s">
                <span class="filepath">%s</span>
                <div class="diff">%s</div>
            </div>""" % (
                diff_file,
                diff_file,
                lines_html_string
            ))
        



# Write the footer to the html file
with open(os.path.realpath(sys.argv[2]), "a+") as html_file:
    html_file.write("""
        </body>
        <script>

window.onload = () => {
    console.log("page is fully loaded");
    files = document.getElementsByClassName("file")

    function searchFilesAndHide (searchString) {
        if (searchString == "") {
            for (file of files) {
                file.style.display = ""
            }
            return true
        }

        for (file of files) {
            file.style.display = file.attributes.filepath.value.includes(searchString) ? "" : "none"
        }

    }

    let debounceTimeout
    document.getElementById("findyourfile").addEventListener("input", event => {
        clearTimeout(debounceTimeout)
        debounceTimeout = setTimeout(() => searchFilesAndHide(event.target.value), 600)
    })

    for (el of document.getElementsByClassName("filepath")){
        el.addEventListener("click", event => {
            event.target.parentElement.classList.toggle("collapsed")
        })
    }
}
        </script>
    </html>
    """)
