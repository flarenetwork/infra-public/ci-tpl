FROM hashicorp/terraform:latest@sha256:1dd96bd77801daa3880ab4943c5cb9d55ad5af51b803dcf6152b8bf8901a0a82

RUN apk add --no-cache git=2.38.5-r0 colordiff=1.0.20-r0 python3=3.10.13-r0 bash=5.2.15-r0
COPY lint-diff2html.py lint-entrypoint.sh /
RUN chmod +x /lint-entrypoint.sh

ENTRYPOINT [ "bash", "-c", "/lint-entrypoint.sh" ]
