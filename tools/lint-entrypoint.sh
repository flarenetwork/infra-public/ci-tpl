#!/bin/bash

cd "$CI_PROJECT_DIR" || exit

if [ -n "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" ]; then
    echo "[i] Env var CI_MERGE_REQUEST_TARGET_BRANCH_NAME set, this is a merge request pipeline, creating a list of changed files."
    git diff --name-only "remotes/origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}...HEAD" > /tmp/changed-files-in-mr.txt
fi


bash "$@"
